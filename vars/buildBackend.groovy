def call(registryDocker, buildContainerName, dockerTag, mailSendTo, telegramBotToken, telegramChatId) {
    try {
        cleanImages(registryDocker, buildContainerName, dockerTag)
        buildImages(registryDocker, buildContainerName, dockerTag)
        sendMessageToTelegram("Docker build Successfully!", telegramBotToken, telegramChatId)
    } catch (Exception e) {
        echo "Build failed, retrying..."
        cleanImages(registryDocker, buildContainerName, dockerTag)
        buildImages(registryDocker, buildContainerName, dockerTag)
        sendMessageToTelegram("Docker build failed!", telegramBotToken, telegramChatId)
        throw e
    }

    try {
        sendMessageToGmail("Docker build completed!", mailSendTo)
    } catch (Exception emailException) {
        echo "Error occurred while sending email notification: ${emailException.message}"
    }
}

def cleanImages( registryDocker, buildContainerName, dockerTag) {
    try {
        sh """
            docker rmi -f ${buildContainerName}:${dockerTag}
            docker rmi -f ${registryDocker}/${buildContainerName}:${dockerTag}
        """
        sendMessageToTelegram("Clean Images Successfully!", telegramBotToken, telegramChatId)
    } catch (Exception cleanupException) {
        sendMessageToTelegram("Docker build Failed!", telegramBotToken, telegramChatId)
        echo "Error occurred while cleaning Docker images: ${cleanupException.message}"
    }
}

def buildImages( registryDocker, buildContainerName, dockerTag) {
    try {
        sh "docker build -t ${buildContainerName}:${dockerTag} -t ${registryDocker}/${buildContainerName}:${dockerTag} ."
    } catch (Exception buildException) {
        echo "Error occurred while building Docker image: ${buildException.message} "
        throw buildException
    }
}

def sendMessageToTelegram( message, telegramBotToken, telegramChatId) {
    try {
        sh "curl -s -X POST https://api.telegram.org/bot${telegramBotToken}/sendMessage -d chat_id=${telegramChatId} -d text='${message}'"
    } catch (Exception telegramException) {
        echo "Error occurred while sending Telegram message: ${telegramException.message}"
    }
}

def sendMessageToGmail(message, mailSendTo) {
    try {
        mail bcc: '', body: message, cc: '', from: '', replyTo: '', subject: 'Hello', to: mailSendTo
    } catch (Exception e) {
        println("Error occurred while sending Gmail message: ${e.message}")
    }
}