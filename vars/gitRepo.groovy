def call(repoUrl, gitCredential, branch, telegramBotToken, telegramChatId) {
// def call(repoUrl, gitCredential, branch, containerNameFrontEnd, containerNameBackEnd) {
    try {
        if (TEST_CHOICE == "main") {
            echo "Clone from main"
            cloneFromGit(repoUrl, gitCredential, branch)
        } else if (TEST_CHOICE == "master") {
            echo "Clone from master"
            cloneFromGit(repoUrl, gitCredential, branch)
        } else {
            error "Invalid TEST_CHOICE value: ${TEST_CHOICE}"
        }
        sendMessageToTelegram("Pull succeeded from branch ${TEST_CHOICE}!", telegramBotToken, telegramChatId)
    } catch (Exception e) {
        sendMessageToTelegram("Pull failed from branch ${TEST_CHOICE}!", telegramBotToken, telegramChatId)
        throw e
    }
}

def cloneFromGit(repoUrl, credentialsId, branch ) {
    try {
        git branch: branch, credentialsId: credentialsId, url: repoUrl
    } catch (Exception e) {
        error "Failed to clone from Git repository: ${e.message}"
    }
}

def sendMessageToTelegram(message, telegramBotToken, telegramChatId) {
    try {
        sh "curl -s -X POST https://api.telegram.org/bot${telegramBotToken}/sendMessage -d chat_id=${telegramChatId} -d text='${message}'"
        println("Telegram message sent successfully!")
    } catch (Exception e) {
        println("Error occurred while sending Telegram message: ${e.message}")
    }
}