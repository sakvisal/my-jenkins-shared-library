def call(minPort, maxPort, registryDocker, buildContainerName, containerName, dockerTag, mailSendTo, telegramBotToken, telegramChatId) {
    try {
        def selectedPort = selectRandomAvailablePort(minPort.toInteger(), maxPort.toInteger())
        if (selectedPort) {
            echo "Selected port: $selectedPort"
            startDockerContainer(selectedPort, registryDocker, buildContainerName, dockerTag, containerName)
            sendMessageToTelegram("Docker Deploy $selectedPort:3000 Successfully!", telegramBotToken, telegramChatId)
            sendMessageToGmail("Docker Deploy $selectedPort:3000 Successfully!", mailSendTo)
            sendIpWithPort(selectedPort, telegramBotToken, telegramChatId)
        } else {
            error "No available ports found in the range $minPort-$maxPort"
        }
    } catch (Exception e) {
        sendMessageToTelegram("Error occurred during Docker deployment: ${e.message}", telegramBotToken, telegramChatId)
        throw e
    }

    try {
        def usedPorts = listPortsInUseForDocker(minPort.toInteger(), maxPort.toInteger())
        if (!usedPorts.isEmpty()) {
            echo "Ports already in use for Docker port mapping on port 3000: ${usedPorts.join(', ')}"
            sendMessageToTelegram("Ports already in use for Docker port mapping on port 3000: ${usedPorts.join(', ')}", telegramBotToken, telegramChatId)
        }
    } catch (Exception e) {
        sendMessageToTelegram("Error occurred while listing used ports: ${e.message}", telegramBotToken, telegramChatId)
        throw e
    }
}

def startDockerContainer(port, registryDocker, containerName, dockerTag, exposedPort) {
    try {
        sh "docker run -d -p ${port}:3000 --name ${containerName} ${registryDocker}/${containerName}:${dockerTag}"
    } catch (Exception e) {
        error "Failed to start Docker container: ${e.message}"
    }
}

def sendMessageToTelegram(message, telegramBotToken, telegramChatId) {
    try {
        sh "curl -s -X POST https://api.telegram.org/bot${telegramBotToken}/sendMessage -d chat_id=${telegramChatId} -d text='${message}'"
    } catch (Exception e) {
        println("Error occurred while sending Telegram message: ${e.message}")
    }
}

def sendMessageToGmail(message, mailSendTo) {
    try {
        mail bcc: '', body: message, cc: '', from: '', replyTo: '', subject: 'Hello', to: mailSendTo
    } catch (Exception e) {
        println("Error occurred while sending Gmail message: ${e.message}")
    }
}

def selectRandomAvailablePort(minPort, maxPort) {
    def numberOfPortsToCheck = maxPort - minPort + 1
    def portsToCheck = (minPort..maxPort).toList()
    Collections.shuffle(portsToCheck)

    for (int i = 0; i < numberOfPortsToCheck; i++) {
        def portToCheck = portsToCheck[i]
        if (isPortAvailable(portToCheck) && !isPortInUseForDocker(portToCheck)) {
            return portToCheck
        }
    }
    return null
}

def isPortAvailable(port) {
    def socket
    try {
        socket = new Socket("localhost", port)
        return false //if the Port is already in use
    } catch (Exception e) {
        return true //if the Port is available
    } finally {
        if (socket) {
            socket.close()
        }
    }
}

def isPortInUseForDocker(port) {
    def dockerPsOutput = sh(script: "docker ps --format '{{.Ports}}'", returnStdout: true).trim()
    return dockerPsOutput.contains(":$port->3000/tcp")
}

def listPortsInUseForDocker(minPort, maxPort) {
    def usedPorts = []
    for (int port = minPort; port <= maxPort; port++) {
        if (isPortInUseForDocker(port)) {
            usedPorts.add(port)
        }
    }
    return usedPorts
}

def sendIpWithPort(port, telegramBotToken, telegramChatId) {
    try {
        def ipAddress = sh(script: 'curl -s ifconfig.me', returnStdout: true).trim()
        def ipWithPort = "${ipAddress}:${port}"
        sendMessageToTelegram(ipWithPort, telegramBotToken, telegramChatId)
    } catch (Exception e) {
        println("Error occurred while sending IP with port information: ${e.message}")
    }
}