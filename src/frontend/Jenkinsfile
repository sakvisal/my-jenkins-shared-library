@Library("my-jenkins-shared-library") _

pipeline {
    agent any

    parameters {
        booleanParam(
            name: 'BUILD_DOCKER',
            defaultValue: true,
            description: 'Build Docker Image'
        )
        booleanParam(
            name: 'DOCKER_DEPLOY',
            defaultValue: true,
            description: 'Docker Deploy'
        )
        choice(
            name: "TEST_CHOICE",
            choices: ["main", "master"],
            description: "Sample multi-choice parameter"
        )
        string(
            name: 'registryDocker',
            defaultValue: 'default',
            description: 'Registry'
        )
        string(
            name: 'buildContainerName',
            defaultValue: 'defaultImage',
            description: 'Container'
        )
        string(
            name: 'containerNameFrontEnd',
            defaultValue: generateContainerNameFrontEnd(),
            description: 'Frontend Container'
        )
        string(
            name: 'dockerTag',
            defaultValue: 'latest',
            description: 'Docker Tag'
        )
        string(
            name: 'repoUrl',
            defaultValue: 'https://github.com/visal-sak/Product-management.git',
            description: 'Repository URL'
        )
    }

    environment {
        telegramBotToken = '6561201622:AAHfh-MaXNwcU17-mUkw57jNeWGkH1LYq4k'
        telegramChatId = '1034126207'
        username = "${params.username}"
        registryDocker = "${params.registryDocker}"
        buildContainerName = "${params.buildContainerName}"
        containerName = "${params.containerName}"
        dockerTag = "${params.dockerTag}"
        mailSendTo = 'sakvisal132@gmail.com'
        minPort = '3000'
        maxPort = '3010'
        repoUrl = "${params.repoUrl}"
        gitCredential = 'password_for_gitlab'
        branch = 'main'
    }

    stages {
        stage('Get Code from SCM') {
            steps {
                echo "TEST_CHOICE is ${TEST_CHOICE}"
                script {
                    gitRepo(
                        repoUrl,
                        gitCredential,
                        branch,
                        telegramBotToken,
                        telegramChatId
                    )
                }
            }
        }

        stage('Build') {
            steps {
                echo "Building Images for deploying"
                script {
                    def dockerfileContent = '''
                        FROM node:lts as build 
                        WORKDIR /app 
                        COPY package*.json ./ 
                        RUN npm install 
                        COPY . . 
                        RUN npm run build 

                        # Production stage
                        FROM node:lts
                        WORKDIR /app
                        COPY --from=build /app ./
                        RUN npm install -g serve
                        EXPOSE 3000
                        CMD ["npm", "start"]
                    '''

                    // Write the Dockerfile content to a file
                    writeFile file: 'Dockerfile', text: dockerfileContent

                    // Build the Docker image using the specified Dockerfile
                    def dockerImage = docker.build("nextjs", "-f Dockerfile .")

                    buildFrontend(
                        registryDocker,
                        buildContainerName,
                        dockerTag,
                        mailSendTo,
                        telegramBotToken,
                        telegramChatId
                    )
                }
            }
        }

        stage('Deploy Docker') {
            steps {
                script {
                    deployFrontend(
                        minPort,
                        maxPort,
                        registryDocker,
                        buildContainerName,
                        containerName,
                        dockerTag,
                        mailSendTo,
                        telegramBotToken,
                        telegramChatId
                    )
                }
            }
        }
    }
}

def generateContainerNameFrontEnd() {
    // Generate a dynamic default value, for example, based on a timestamp or a random value
    return "frontend-${new Date().getTime()}"
}